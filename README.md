# Lab7 - Rollbacks and reliability

## Homework

### Part 1 (2/3 points)

Used sql transactions (BEGIN-COMMIT-ROLLBACK)

### Part 2 (3/3 points)

- Create a new table for the player inventory.
```sh
CREATE TABLE Inventory (
    username TEXT REFERENCES Player(username) NOT NULL,
    product TEXT REFERENCES Shop(product) NOT NULL,
    amount INT CHECK(amount >= 0),
    UNIQUE(username, product)
);

```

- Add queries to create, read, and update an inventory entry

- Include inventory checks and updates in the transaction



### Extra part (1.5 points)

In the scenario where a transaction is committed but the function still fails due to an external issue such as electricity shortage or a broken client-server connection, the transaction would be left in a partially completed state.

If clients attempt to retry the request, the database may reject the transaction due to its partially completed state, which could cause data inconsistency or errors.

To handle this scenario, we could implement a technique called "transaction retry" or "transaction roll-forward." This involves keeping track of transactions that have been partially completed, and allowing them to be retried and completed at a later time.

For example, you could use a technique called "two-phase commit" to ensure that a transaction is only committed if all parties involved in the transaction have successfully completed their portion of the work. If a failure occurs during the commit phase, the transaction can be retried at a later time.

Another approach is to use database replication to ensure that multiple copies of the data exist in different locations. If one location fails, another location can take over and continue processing transactions.

### Extra part (1.5 points)

In the scenario where there are multiple storages involved and one of them does not support rollback or undo, it can be challenging to ensure consistency and correctness.

One approach is to use a compensation pattern, where you perform a compensating action to undo the effect of the original action if necessary. For example, if you decreased the balance in the database but the money withdrawal failed, you could use a compensation action to increase the balance back to its original value.

Another approach is to use a Saga pattern, where you break down the overall transaction into smaller, more manageable sub-transactions. Each sub-transaction corresponds to an operation on a single storage, and a Saga orchestrates the sub-transactions to ensure consistency. In the event of a failure, the Saga can coordinate a compensating action to undo the effects of the completed sub-transactions.

In the specific example provided, one possible approach is to use a Saga to coordinate the two operations: decreasing the balance in the database and starting the money withdrawal in the message queue. The Saga would ensure that both operations complete successfully or both are undone if one fails. If the money withdrawal takes a long time, you could consider using a timeout and retry mechanism to ensure that the Saga does not get stuck waiting indefinitely.

Another option would be to ensure that the message queue supports some form of atomicity or transactionality, so that you can ensure that the message is either completely processed or not processed at all. This can help to ensure consistency across different storage systems.